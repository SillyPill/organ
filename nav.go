package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	times "gopkg.in/djherbis/times.v1"
)

type linkState byte

const (
	notLink linkState = iota
	working
	broken
)

type node struct {
	os.FileInfo
	linkState  linkState
	nodeName   string
	path       string
	dirPath    string
	dirCount   int
	accessTime time.Time
	changeTime time.Time
	ext        string
	depth      int
}

func (t *node) nodePath() string {
	if t.path != "" {
		return t.path
	} else {
		return t.dirPath
	}
}

func (t *node) IsDir() bool {
	return t.dirPath != ""
}

func readNodes(path string, depth int) ([]*node, error) {

	// Open path
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	names, err := f.Readdirnames(-1) // Get the list of files in the path
	f.Close()

	mNodes := make(map[string]*node)

	for _, fname := range names {

		suffix := gOpts.suffix

		nodepath := filepath.Join(path, fname)
		lstat, err := os.Lstat(nodepath)
		if os.IsNotExist(err) {
			continue
		}
		if err != nil {
			return nil, err
		}

		nodePath := fname
		if lstat.IsDir() {
			if strings.HasSuffix(fname, suffix) {
				nodePath = strings.TrimSuffix(fname, suffix)
			}
		}

		var linkState linkState

		ts := times.Get(lstat)
		at := ts.AccessTime()
		var ct time.Time
		// from times docs: ChangeTime() panics unless HasChangeTime() is true
		if ts.HasChangeTime() {
			ct = ts.ChangeTime()
		} else {
			// fall back to ModTime if ChangeTime cannot be determined
			ct = lstat.ModTime()
		}

		// returns an empty string if extension could not be determined
		// i.e. directories, filenames without extensions
		ext := filepath.Ext(nodepath)

		// If thing is not already in the map, create a new thing and insert it
		if _, ok := mNodes[nodePath]; !ok {

			if lstat.IsDir() {
				mNodes[nodePath] = &node{
					FileInfo:   lstat,
					linkState:  linkState,
					nodeName:   nodePath,
					dirPath:    nodepath,
					dirCount:   -1,
					accessTime: at,
					changeTime: ct,
					ext:        ext,
					depth:      depth,
				}
			} else {
				mNodes[nodePath] = &node{
					FileInfo:   lstat,
					linkState:  linkState,
					nodeName:   nodePath,
					path:       nodepath,
					dirCount:   -1,
					accessTime: at,
					changeTime: ct,
					ext:        ext,
					depth:      depth,
				}
			}

		} else {
			if lstat.IsDir() {
				mNodes[nodePath].dirPath = nodepath
			} else {
				mNodes[nodePath].path = nodepath
			}
		}

	}

	// return a slice of "nodes"
	nodes := make([]*node, 0, len(names))
	for _, value := range mNodes {
		nodes = append(nodes, value)
	}

	return nodes, err
}

type menu struct {
	loading  bool      // directory is loading from disk
	loadTime time.Time // current loading or last load time
	ind      int       // index of current entry in nodes
	pos      int       // position of current entry in ui
	path     string    // full path of directory
	nodes    []*node   // displayed nodes in directory including or excluding hidden ones
	allNodes []*node   // all nodes in directory including hidden ones (same array as nodes)
	sortType sortType  // sort method and options from last sort
	noPerm   bool      // whether lf has no permission to open the directory
	hoff     int
}

func newMenu(path string, expandedCache map[string]bool) *menu {
	time := time.Now()

	nodes, err := readNodes(path, 0)
	sortNodes(&nodes)

	// If any of the new "Nodes" were expanded before, expand them
	for i := 0; i < len(nodes); i++ {

		if nodes[i].dirPath != "" {
			if e, ok := expandedCache[nodes[i].dirPath]; !ok || !e {
				continue
			}
			subNodes, err := readNodes(nodes[i].dirPath, nodes[i].depth+1)
			if err != nil {
				log.Printf("reading directory: %s", err)
			}
			sortNodes(&subNodes)

			nodes = append(nodes[:i+1], append(subNodes, nodes[i+1:]...)...)
		}
	}
	if err != nil {
		log.Printf("reading directory: %s", err)
	}

	return &menu{
		loadTime: time,
		path:     path,
		nodes:    nodes,
		allNodes: nodes,
		noPerm:   os.IsPermission(err),
	}
}

func sortNodes(n *[]*node) {

	nodes := *n

	switch gOpts.sortType.method {
	case naturalSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			return naturalLess(strings.ToLower(nodes[i].Name()), strings.ToLower(nodes[j].Name()))
		})
	case nameSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			return strings.ToLower(nodes[i].Name()) < strings.ToLower(nodes[j].Name())
		})
	case sizeSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			return nodes[i].Size() < nodes[j].Size()
		})
	case timeSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			return nodes[i].ModTime().Before(nodes[j].ModTime())
		})
	case atimeSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			return nodes[i].accessTime.Before(nodes[j].accessTime)
		})
	case ctimeSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			return nodes[i].changeTime.Before(nodes[j].changeTime)
		})
	case extSort:
		sort.SliceStable(nodes, func(i, j int) bool {
			leftExt := strings.ToLower(nodes[i].ext)
			rightExt := strings.ToLower(nodes[j].ext)

			// if the extension could not be determined (directories, things without)
			// use a zero byte so that these things can be ranked higher
			if leftExt == "" {
				leftExt = "\x00"
			}
			if rightExt == "" {
				rightExt = "\x00"
			}

			// in order to also have natural sorting with the filenames
			// combine the name with the ext but have the ext at the front
			left := leftExt + strings.ToLower(nodes[i].Name())
			right := rightExt + strings.ToLower(nodes[j].Name())
			return left < right
		})
	}

	if gOpts.sortType.option&reverseSort != 0 {
		for i, j := 0, len(nodes)-1; i < j; i, j = i+1, j-1 {
			nodes[i], nodes[j] = nodes[j], nodes[i]
		}
	}

	if gOpts.sortType.option&dirfirstSort != 0 {
		sort.SliceStable(nodes, func(i, j int) bool {
			if nodes[i].IsDir() == nodes[j].IsDir() {
				return i < j
			}
			return nodes[i].IsDir()
		})
	}

	// // when hidden option is disabled, we move hidden things to the
	// // beginning of our thing list and then set the beginning of displayed
	// // things to the first non-hidden thing in the list
	// if gOpts.sortType.option&hiddenSort == 0 {
	// 	sort.SliceStable(things, func(i, j int) bool {
	// 		if isHidden(things[i]) && isHidden(things[j]) {
	// 			return i < j
	// 		}
	// 		return isHidden(things[i])
	// 	})
	// 	for i, f := range things {
	// 		if !isHidden(f) {
	// 			things = things[i:]
	// 			return
	// 		}
	// 	}
	// 	things = things[len(things):]
	// }

}

func (dir *menu) name() string {
	if len(dir.nodes) == 0 {
		return ""
	}
	return dir.nodes[dir.ind].Name()
}

func (dir *menu) sel(name string, height int) {
	if len(dir.nodes) == 0 {
		dir.ind, dir.pos = 0, 0
		return
	}

	dir.ind = min(dir.ind, len(dir.nodes)-1)

	if dir.nodes[dir.ind].Name() != name {
		for i, f := range dir.nodes {
			if f.Name() == name {
				dir.ind = i
				break
			}
		}
	}

	edge := min(min(height/2, gOpts.scrolloff), len(dir.nodes)-dir.ind-1)
	dir.pos = min(dir.ind, height-edge-1)
}

type renameEntry struct {
	oldPathTo string
	newPathTo string
}

type nav struct {
	menu            menu
	copyBytes       int64
	copyTotal       int64
	copyUpdate      int
	moveCount       int
	moveTotal       int
	moveUpdate      int
	deleteCount     int
	deleteTotal     int
	deleteUpdate    int
	copyBytesChan   chan int64
	copyTotalChan   chan int64
	moveCountChan   chan int
	moveTotalChan   chan int
	deleteCountChan chan int
	deleteTotalChan chan int
	menuChan        chan *menu
	previewChan     chan *preview
	regCache        map[string]*preview
	saves           map[string]bool
	marks           map[string]string
	expandedCache   map[string]bool
	renameCache     []renameEntry
	selections      map[string]int
	selectionInd    int
	height          int
	find            string
	findBack        bool
	search          string
	searchBack      bool
	searchInd       int
	searchPos       int
}

func (nav *nav) loadDir(path string) *menu {

	go func() {
		d := newMenu(path, nav.expandedCache)
		d.ind, d.pos = nav.menu.ind, nav.menu.pos
		nav.menuChan <- d
	}()

	d := &menu{loading: true, path: path, sortType: gOpts.sortType}
	nav.menu = *d

	return d
}

func newNav(height int) *nav {
	wd, err := os.Getwd()
	if err != nil {
		log.Printf("getting current directory: %s", err)
	}

	nav := &nav{
		copyBytesChan:   make(chan int64, 1024),
		copyTotalChan:   make(chan int64, 1024),
		moveCountChan:   make(chan int, 1024),
		moveTotalChan:   make(chan int, 1024),
		deleteCountChan: make(chan int, 1024),
		deleteTotalChan: make(chan int, 1024),
		menuChan:        make(chan *menu),
		previewChan:     make(chan *preview),
		regCache:        make(map[string]*preview),
		saves:           make(map[string]bool),
		marks:           make(map[string]string),
		expandedCache:   make(map[string]bool),
		renameCache:     make([]renameEntry, 2),
		selections:      make(map[string]int),
		selectionInd:    0,
		height:          height,
	}

	nav.menu = *nav.loadDir(wd)

	return nav
}

func (nav *nav) renew() {

	go func(d menu) {
		s, err := os.Stat(d.path)
		if err != nil {
			log.Printf("getting directory info: %s", err)
			return
		}
		if d.loadTime.After(s.ModTime()) {
			return
		}
		d.loadTime = time.Now()
		nd := newMenu(d.path, nav.expandedCache)
		nav.menuChan <- nd
	}(nav.menu)

	for m := range nav.selections {
		if _, err := os.Lstat(m); os.IsNotExist(err) {
			delete(nav.selections, m)
		}
	}
	if len(nav.selections) == 0 {
		nav.selectionInd = 0
	}
}

func (nav *nav) reload() error {
	nav.regCache = make(map[string]*preview)

	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("Error loading directory: %s", err)
	}

	curr, err := nav.currThing()

	nav.menu = *nav.loadDir(wd)

	if err != nil {
		return fmt.Errorf("Err: %s", err)
	}
	menu := nav.menu
	menu.nodes = append(menu.nodes, curr)

	return nil
}

func (nav *nav) position() {
	path := nav.menu.path
	nav.menu.sel(filepath.Base(path), nav.height)
}

func (nav *nav) previewCurr() {
	curr, err := nav.currThing()
	if err != nil {
		return
	}

	thingPath := curr.nodePath()

    reg := &preview{
        loadTime: time.Now(),
        path:     thingPath,
        voff:     0,
    }

	var reader io.Reader

    cmd := exec.Command(gOpts.previewer, thingPath, strconv.Itoa(nav.height))

    out, err := cmd.StdoutPipe()
    if err != nil {
        log.Printf("previewing thing: %s", err)
    }

    if err := cmd.Start(); err != nil {
        log.Printf("previewing thing: %s", err)
    }

    defer cmd.Wait()
    defer out.Close()
    reader = out


	buf := bufio.NewScanner(reader)

	for i := 0; buf.Scan(); i++ {
		for _, r := range buf.Text() {
			if r == 0 {
				reg.lines = []string{"\033[7mbinary\033[0m"}
				nav.previewChan <- reg
				return
			}
		}
		reg.lines = append(reg.lines, buf.Text())
	}

	if buf.Err() != nil {
		log.Printf("loading thing: %s", buf.Err())
	}

	nav.previewChan <- reg
}

func (nav *nav) loadCurrPreview() *preview {

	t, err := nav.currThing()
	if err != nil {
		return nil
	}

	thingPath := t.nodePath()

	// Check cache for preview data, if it doesn't exist create preview data
	r, ok := nav.regCache[thingPath]
	if !ok {
        r := &preview{
            loading: true,
            loadTime: time.Now(),
            path: thingPath, voff: 0,
        }
		nav.regCache[thingPath] = r
		go nav.previewCurr()
		return r
	}

    nav.checkPrev(r)

    return r
}

func (nav *nav) currThingGoto(n int) error {

	if n < 0 {
		return fmt.Errorf("Can't go to negative position LMAO")
	}

	// Get curr file from nav
	t, err := nav.currThing()
	if err != nil {
		return nil
	}

	thingPath := t.nodePath()

	// Check cache for preview data, if it doesn't exist create preview data
	r, ok := nav.regCache[thingPath]
	if !ok {
		return fmt.Errorf("Preview not Loaded")
	}

	r.voff = n

	return nil
}

func (nav *nav) currThingPreviewScroll(n int) error {

	// Get curr file from nav
	t, err := nav.currThing()
	if err != nil {
		return nil
	}

	thingPath := t.nodePath()

	// Check cache for preview data, if it doesn't exist create preview data
	r, ok := nav.regCache[thingPath]
	if !ok {
		return fmt.Errorf("Preview not Loaded")
	}

	r.voff = max(r.voff+n, 0)

	return nil
}

func (nav *nav) loadThing(path string) *preview {
	r, ok := nav.regCache[path]
	if !ok {
		go nav.previewCurr()
		r := &preview{loading: true, path: path}
		nav.regCache[path] = r
		return r
	}

	s, err := os.Stat(r.path)
	if err != nil {
		return r
	}

	if s.ModTime().After(r.loadTime) {
		r.loadTime = time.Now()
		go nav.previewCurr()
	}

	return r
}

func (nav *nav) checkPrev(pr *preview) {
	s, err := os.Stat(pr.path)
	if err != nil {
		return
	}

	if s.ModTime().After(pr.loadTime) {
		pr.loadTime = time.Now()
		go nav.previewCurr()
	}
}

func (nav *nav) sort() {

	things, err := readNodes(nav.menu.path, 0)
	if err != nil {
		log.Printf("reading directory: %s", err)
	}
	sortNodes(&things)

	// If any of the new "Things" were expanded before, expand them
	for i := 0; i < len(things); i++ {

		if things[i].dirPath != "" {
			if e, ok := nav.expandedCache[things[i].dirPath]; !ok || !e {
				continue
			}
			subThings, err := readNodes(things[i].dirPath, things[i].depth+1)
			if err != nil {
				log.Printf("reading directory: %s", err)
			}

			sortNodes(&subThings)

			things = append(things[:i+1], append(subThings, things[i+1:]...)...)
		}
	}
	nav.menu.nodes = things
	nav.menu.sel(nav.menu.name(), nav.height)
}

func (nav *nav) up(dist int) {

	if nav.menu.ind == 0 {
		if gOpts.wrapscroll {
			nav.bottom()
		}
		return
	}

	nav.menu.ind -= dist
	nav.menu.ind = max(0, nav.menu.ind)

	nav.menu.pos -= dist
	edge := min(min(nav.height/2, gOpts.scrolloff), nav.menu.ind)
	nav.menu.pos = max(nav.menu.pos, edge)
}

func (nav *nav) down(dist int) {
	dir := &nav.menu

	maxind := len(dir.nodes) - 1

	if dir.ind >= maxind {
		if gOpts.wrapscroll {
			nav.top()
		}
		return
	}

	dir.ind += dist
	dir.ind = min(maxind, dir.ind)

	dir.pos += dist
	edge := min(min(nav.height/2, gOpts.scrolloff), maxind-dir.ind)

	// use a smaller value when the height is even and scrolloff is maxed
	// in order to stay at the same row as much as possible while up/down
	edge = min(edge, nav.height/2+nav.height%2-1)

	dir.pos = min(dir.pos, nav.height-edge-1)
	dir.pos = min(dir.pos, maxind)
}

func (nav *nav) expandCurr() error {
	curr, err := nav.currThing()
	if err != nil {
		return fmt.Errorf("open: %s", err)
	}

	path := curr.dirPath

	dir, err := readNodes(path, curr.depth+1)
	if err != nil {
		return fmt.Errorf("open: %s", err)
	}

	if len(dir) == 0 {
		return fmt.Errorf("Empty directory: %s", path)
	}

	sortNodes(&dir)

	nav.expandedCache[path] = true

	// If any of the new nodes were expanded before, expand them
	for i := 0; i < len(dir); i++ {

		if dir[i].dirPath != "" {
			if e, ok := nav.expandedCache[dir[i].dirPath]; !ok || !e {
				continue
			}
			subThings, err := readNodes(dir[i].dirPath, dir[i].depth+1)
			if err != nil {
				log.Printf("reading directory: %s", err)
			}

			sortNodes(&subThings)

			dir = append(dir[:i+1], append(subThings, dir[i+1:]...)...)
		}
	}

	newThings := append(
		nav.menu.nodes[:nav.menu.ind+1],
		append(
			dir,
			nav.menu.nodes[nav.menu.ind+1:]...,
		)...,
	)

	nav.menu.nodes = newThings

	return nil
}

func (nav *nav) collapseCurr() error {
	curr, err := nav.currThing()
	if err != nil {
		return fmt.Errorf("open: %s", err)
	}

	path := curr.dirPath

	if e, ok := nav.expandedCache[curr.dirPath]; !ok || !e {
		return fmt.Errorf("Nothing to Collapse")
	}

	count := 0
	for i := nav.menu.ind + 1; i < len(nav.menu.nodes) &&
		nav.menu.nodes[i].depth > nav.menu.nodes[nav.menu.ind].depth; i++ {
		count++
	}

	nav.menu.nodes = append(
		nav.menu.nodes[:nav.menu.ind+1],
		nav.menu.nodes[nav.menu.ind+count+1:]...,
	)

	nav.expandedCache[path] = false

	return nil
}

func (nav *nav) top() {
	dir := &nav.menu

	dir.ind = 0
	dir.pos = 0
}

func (nav *nav) bottom() {
	dir := &nav.menu

	dir.ind = len(dir.nodes) - 1
	dir.pos = min(dir.ind, nav.height-1)
}

func (nav *nav) toggleSelection(path string) {
	if _, ok := nav.selections[path]; ok {
		delete(nav.selections, path)
		if len(nav.selections) == 0 {
			nav.selectionInd = 0
		}
	} else {
		nav.selections[path] = nav.selectionInd
		nav.selectionInd++
	}
}

func (nav *nav) toggle() {
	curr, err := nav.currThing()
	if err != nil {
		return
	}

	log.Printf("Toggling %s", curr.nodePath())
	if curr.dirPath != "" {
		nav.toggleSelection(curr.dirPath)
	}
	if curr.path != "" {
		nav.toggleSelection(curr.path)
	}

	nav.down(1)
}

func (nav *nav) invert() {
	for _, f := range nav.menu.nodes {
		path := f.nodePath()
		nav.toggleSelection(path)
	}
}

func (nav *nav) unselect() {
	nav.selections = make(map[string]int)
	nav.selectionInd = 0
}

func (nav *nav) save(cp bool) error {
	list, err := nav.currFileOrSelections()
	if err != nil {
		return err
	}

	if err := saveFiles(list, cp); err != nil {
		return err
	}

	nav.saves = make(map[string]bool)
	for _, f := range list {
		nav.saves[f] = cp
	}

	return nil
}

func (nav *nav) copyAsync(ui *ui, srcs []string, dstDir string) {
	echo := &callExpr{"echoerr", []string{""}, 1}

	_, err := os.Stat(dstDir)
	if os.IsNotExist(err) {
		echo.args[0] = err.Error()
		ui.exprChan <- echo
		return
	}

	total, err := copySize(srcs)
	if err != nil {
		echo.args[0] = err.Error()
		ui.exprChan <- echo
		return
	}

	nav.copyTotalChan <- total

	nums, errs := copyAll(srcs, dstDir)

	errCount := 0
loop:
	for {
		select {
		case n := <-nums:
			nav.copyBytesChan <- n
		case err, ok := <-errs:
			if !ok {
				break loop
			}
			errCount++
			echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
			ui.exprChan <- echo
		}
	}

	nav.copyTotalChan <- -total

	if err := remote("send load"); err != nil {
		errCount++
		echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
		ui.exprChan <- echo
	}

    if errCount == 0 {
		ui.exprChan <- &callExpr{"echo", []string{"\033[0;32mCopied successfully\033[0m"}, 1}
	}
}

func (nav *nav) moveAsync(ui *ui, srcs []string, dstDir string) {
	echo := &callExpr{"echoerr", []string{""}, 1}

	_, err := os.Stat(dstDir)
	if os.IsNotExist(err) {
		echo.args[0] = err.Error()
		ui.exprChan <- echo
		return
	}

	nav.moveTotalChan <- len(srcs)

	errCount := 0
	for _, src := range srcs {
		nav.moveCountChan <- 1

		srcStat, err := os.Stat(src)
		if err != nil {
			errCount++
			echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
			ui.exprChan <- echo
			continue
		}

		dst := filepath.Join(dstDir, filepath.Base(src))

		dstStat, err := os.Stat(dst)
		if os.SameFile(srcStat, dstStat) {
			errCount++
			echo.args[0] = fmt.Sprintf("[%d] rename %s %s: source and destination are the same file", errCount, src, dst)
			ui.exprChan <- echo
			continue
		} else if !os.IsNotExist(err) {
			var newPath string
			for i := 1; !os.IsNotExist(err); i++ {
				newPath = fmt.Sprintf("%s.~%d~", dst, i)
				_, err = os.Lstat(newPath)
			}
			dst = newPath
		}

		if err := os.Rename(src, dst); err != nil {
			if errCrossDevice(err) {
				total, err := copySize([]string{src})
				if err != nil {
					echo.args[0] = err.Error()
					ui.exprChan <- echo
					continue
				}

				nav.copyTotalChan <- total

				nums, errs := copyAll([]string{src}, dstDir)

				oldCount := errCount
			loop:
				for {
					select {
					case n := <-nums:
						nav.copyBytesChan <- n
					case err, ok := <-errs:
						if !ok {
							break loop
						}
						errCount++
						echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
						ui.exprChan <- echo
					}
				}

				nav.copyTotalChan <- -total

				if errCount == oldCount {
					if err := os.RemoveAll(src); err != nil {
						errCount++
						echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
						ui.exprChan <- echo
					}
				}
			} else {
				errCount++
				echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
				ui.exprChan <- echo
			}
		}
	}

	nav.moveTotalChan <- -len(srcs)

	if err := remote("send load"); err != nil {
		errCount++
		echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
		ui.exprChan <- echo
	}

	if errCount == 0 {
		ui.exprChan <- &callExpr{"echo", []string{"\033[0;32mMoved successfully\033[0m"}, 1}
	}
}

func (nav *nav) paste(ui *ui) error {
	srcs, cp, err := loadFiles()
	if err != nil {
		return err
	}

	if len(srcs) == 0 {
		return errors.New("no thing in copy/cut buffer")
	}

	curr, err := nav.currThing()
	if err != nil {
		return fmt.Errorf("opening: %s", err)
	}

	dstDir := filepath.Dir(curr.nodePath())

	if cp {
		go nav.copyAsync(ui, srcs, dstDir)
	} else {
		go nav.moveAsync(ui, srcs, dstDir)
	}

	if err := saveFiles(nil, false); err != nil {
		return fmt.Errorf("clearing copy/cut buffer: %s", err)
	}

	if err := remote("send sync"); err != nil {
		return fmt.Errorf("paste: %s", err)
	}

	return nil
}

func (nav *nav) del(ui *ui) error {
	list, err := nav.currFileOrSelections()
	if err != nil {
		return err
	}

	go func() {
		echo := &callExpr{"echoerr", []string{""}, 1}
		errCount := 0

		nav.deleteTotalChan <- len(list)

		for _, path := range list {
			nav.deleteCountChan <- 1

			if err := os.RemoveAll(path); err != nil {
				errCount++
				echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
				ui.exprChan <- echo
			}
		}

		nav.deleteTotalChan <- -len(list)

		if err := remote("send load"); err != nil {
			errCount++
			echo.args[0] = fmt.Sprintf("[%d] %s", errCount, err)
			ui.exprChan <- echo
		}
	}()

	return nil
}

func (nav *nav) rename() error {

	for _, e := range nav.renameCache {

		oldPath := e.oldPathTo
		newPath := e.newPathTo
		dir, _ := filepath.Split(newPath)
		os.MkdirAll(dir, os.ModePerm)

		if _, err := os.Stat(newPath); err == nil { // thing exists
			if err := os.Remove(newPath); err != nil {
				return err
			}
		}

		if err := os.Rename(oldPath, newPath); err != nil {
			return err
		}

		// // TODO: change selection
		// if err := nav.sel(newPath); err != nil {
		//     return err
		// }

	}

	return nil
}

func (nav *nav) sync() error {
	list, cp, err := loadFiles()
	if err != nil {
		return err
	}

	nav.saves = make(map[string]bool)
	for _, f := range list {
		nav.saves[f] = cp
	}

	return nav.readMarks()
}

func (nav *nav) cd(wd string) error {
	wd = replaceTilde(wd)
	wd = filepath.Clean(wd)

	if !filepath.IsAbs(wd) {
		wd = filepath.Join(nav.menu.path, wd)
	}

	if err := os.Chdir(wd); err != nil {
		return fmt.Errorf("cd: %s", err)
	}

	nav.menu = *nav.loadDir(wd)

	return nil
}

func (nav *nav) sel(path string) error {
	path = replaceTilde(path)
	path = filepath.Clean(path)

    _, err := os.Lstat(path)
	if err != nil {
		return fmt.Errorf("select: %s", err)
	}

	wd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("select: %s", err)
	}

	path, err = filepath.Abs(path)
	if err != nil {
		return fmt.Errorf("select: %s", err)
	}

	// TODO: filepath.HasPrefix is broken and deprecated
	// There is talks of implementing HasFilepathPrefix()
	// Here : https://github.com/golang/go/issues/18358
	// Or maybe use https://godoc.org/github.com/golang/dep/internal/fs
	if !filepath.HasPrefix(path, wd) {
		return fmt.Errorf("Selected file not in working directory. Do a cd first")
	}

	selStack := []string{}

	se := path
	for se != wd {
		selStack = append(selStack, se)
		se = filepath.Dir(se)

	}

    currDepth := 0

    // FIXME: Dumb linear search.
    // This whole function is a fixme
    for i := 0; i < len(nav.menu.nodes); i++ {

        n := nav.menu.nodes[i]
        if n.depth > currDepth {
            continue
        }

        l := len(selStack)

		if l == 1 {

            if n.path != selStack[0] && n.dirPath != selStack[0] {
                continue
            }

            nav.menu.ind = i
            break

		} else {

            if n.dirPath != selStack[l-1] {
                continue
            }


            nav.menu.ind = i
            if ! nav.expandedCache[n.dirPath] == true {
                nav.expandCurr()
            }

            currDepth++

            selStack = selStack[:l-1]
            continue

		}

	}

    edge := min(min(nav.height/2, gOpts.scrolloff), nav.menu.ind)
    nav.menu.pos = max(nav.menu.pos, edge)

	return nil
}

func (nav *nav) globSel(pattern string, invert bool) error {
	curDir := nav.menu
	anyMatched := false

	for i := 0; i < len(curDir.nodes); i++ {
		match, err := filepath.Match(pattern, curDir.nodes[i].Name())
		if err != nil {
			return fmt.Errorf("glob-select: %s", err)
		}
		if match {
			anyMatched = true
			fpath := filepath.Join(curDir.path, curDir.nodes[i].Name())
			if _, ok := nav.selections[fpath]; ok == invert {
				nav.toggleSelection(fpath)
			}
		}
	}
	if !anyMatched {
		return fmt.Errorf("glob-select: pattern not found: %s", pattern)
	}
	return nil
}

func findMatch(name, pattern string) bool {
	if gOpts.ignorecase {
		lpattern := strings.ToLower(pattern)
		if !gOpts.smartcase || lpattern == pattern {
			pattern = lpattern
			name = strings.ToLower(name)
		}
	}
	if gOpts.ignoredia {
		lpattern := removeDiacritics(pattern)
		if !gOpts.smartdia || lpattern == pattern {
			pattern = lpattern
			name = removeDiacritics(name)
		}
	}
	if gOpts.anchorfind {
		return strings.HasPrefix(name, pattern)
	}
	return strings.Contains(name, pattern)
}

func (nav *nav) findSingle() int {
	count := 0
	index := 0
	menu := nav.menu
	for i := 0; i < len(menu.nodes); i++ {
		if findMatch(menu.nodes[i].Name(), nav.find) {
			count++
			if count > 1 {
				return count
			}
			index = i
		}
	}
	if count == 1 {
		if index > menu.ind {
			nav.down(index - menu.ind)
		} else {
			nav.up(menu.ind - index)
		}
	}
	return count
}

func (nav *nav) findNext() bool {
	menu := nav.menu
	for i := menu.ind + 1; i < len(menu.nodes); i++ {
		if findMatch(menu.nodes[i].Name(), nav.find) {
			nav.down(i - menu.ind)
			return true
		}
	}
	if gOpts.wrapscan {
		for i := 0; i < menu.ind; i++ {
			if findMatch(menu.nodes[i].Name(), nav.find) {
				nav.up(menu.ind - i)
				return true
			}
		}
	}
	return false
}

func (nav *nav) findPrev() bool {
	menu := nav.menu
	for i := menu.ind - 1; i >= 0; i-- {
		if findMatch(menu.nodes[i].Name(), nav.find) {
			nav.up(menu.ind - i)
			return true
		}
	}
	if gOpts.wrapscan {
		for i := len(menu.nodes) - 1; i > menu.ind; i-- {
			if findMatch(menu.nodes[i].Name(), nav.find) {
				nav.down(i - menu.ind)
				return true
			}
		}
	}
	return false
}

func searchMatch(name, pattern string) (matched bool, err error) {
	if gOpts.ignorecase {
		lpattern := strings.ToLower(pattern)
		if !gOpts.smartcase || lpattern == pattern {
			pattern = lpattern
			name = strings.ToLower(name)
		}
	}
	if gOpts.ignoredia {
		lpattern := removeDiacritics(pattern)
		if !gOpts.smartdia || lpattern == pattern {
			pattern = lpattern
			name = removeDiacritics(name)
		}
	}
	if gOpts.globsearch {
		return filepath.Match(pattern, name)
	}
	return strings.Contains(name, pattern), nil
}

func (nav *nav) searchNext() error {
	menu := nav.menu
	for i := menu.ind + 1; i < len(menu.nodes); i++ {
		matched, err := searchMatch(menu.nodes[i].Name(), nav.search)
		if err != nil {
			return err
		}
		if matched {
			nav.down(i - menu.ind)
			return nil
		}
	}
	if gOpts.wrapscan {
		for i := 0; i < menu.ind; i++ {
			matched, err := searchMatch(menu.nodes[i].Name(), nav.search)
			if err != nil {
				return err
			}
			if matched {
				nav.up(menu.ind - i)
				return nil
			}
		}
	}
	return nil
}

func (nav *nav) searchPrev() error {
	menu := nav.menu
	for i := menu.ind - 1; i >= 0; i-- {
		matched, err := searchMatch(menu.nodes[i].Name(), nav.search)
		if err != nil {
			return err
		}
		if matched {
			nav.up(menu.ind - i)
			return nil
		}
	}
	if gOpts.wrapscan {
		for i := len(menu.nodes) - 1; i > menu.ind; i-- {
			matched, err := searchMatch(menu.nodes[i].Name(), nav.search)
			if err != nil {
				return err
			}
			if matched {
				nav.down(i - menu.ind)
				return nil
			}
		}
	}
	return nil
}

func (nav *nav) removeMark(mark string) error {
	if _, ok := nav.marks[mark]; ok {
		delete(nav.marks, mark)
		return nil
	}
	return fmt.Errorf("no such mark")
}

func (nav *nav) readMarks() error {
	nav.marks = make(map[string]string)
	f, err := os.Open(gMarksPath)
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return fmt.Errorf("opening marks thing: %s", err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		toks := strings.SplitN(scanner.Text(), ":", 2)
		if _, ok := nav.marks[toks[0]]; !ok {
			nav.marks[toks[0]] = toks[1]
		}
	}

	if err := scanner.Err(); err != nil {
		return fmt.Errorf("reading marks thing: %s", err)
	}

	return nil
}

func (nav *nav) writeMarks() error {
	if err := os.MkdirAll(filepath.Dir(gMarksPath), os.ModePerm); err != nil {
		return fmt.Errorf("creating data directory: %s", err)
	}

	f, err := os.Create(gMarksPath)
	if err != nil {
		return fmt.Errorf("creating marks thing: %s", err)
	}
	defer f.Close()

	var keys []string
	for k := range nav.marks {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		_, err = f.WriteString(fmt.Sprintf("%s:%s\n", k, nav.marks[k]))
		if err != nil {
			return fmt.Errorf("writing marks thing: %s", err)
		}
	}

	return nil
}

func (nav *nav) currThing() (*node, error) {

	if len(nav.menu.nodes) == 0 {
		return nil, fmt.Errorf("empty directory")
	}
	return nav.menu.nodes[nav.menu.ind], nil
}

type indexedSelections struct {
	paths   []string
	indices []int
}

func (m indexedSelections) Len() int { return len(m.paths) }

func (m indexedSelections) Swap(i, j int) {
	m.paths[i], m.paths[j] = m.paths[j], m.paths[i]
	m.indices[i], m.indices[j] = m.indices[j], m.indices[i]
}

func (m indexedSelections) Less(i, j int) bool { return m.indices[i] < m.indices[j] }

func (nav *nav) currSelections() []string {
	paths := make([]string, 0, len(nav.selections))
	indices := make([]int, 0, len(nav.selections))
	for path, index := range nav.selections {
		paths = append(paths, path)
		indices = append(indices, index)
	}
	sort.Sort(indexedSelections{paths: paths, indices: indices})
	return paths
}

func (nav *nav) currFileOrSelections() (list []string, err error) {
	if len(nav.selections) == 0 {
		curr, err := nav.currThing()
		if err != nil {
			return nil, errors.New("no thing selected")
		}

		selections := []string{curr.nodePath()}
		if curr.dirPath != "" && curr.path != "" {
			selections = append(selections, curr.dirPath)
		}
		return selections, nil
	}

	return nav.currSelections(), nil
}

func (nav *nav) horizontalScroll(amount int) {
	nav.menu.hoff = max(nav.menu.hoff+amount, 0)
}

// Creates a new file and returns a corresponding thing
func newThing(basepath string, name string) (*node, error) {

	path := filepath.Join(basepath, name)
	f, err := os.Create(path)
	if err != nil {
		return nil, fmt.Errorf("Err: %s", err)
	}
	f.Close()

	lstat, err := os.Lstat(f.Name())
	if err != nil {
		return nil, fmt.Errorf("Err: %s", err)
	}

	var linkState linkState

	ts := times.Get(lstat)
	at := ts.AccessTime()
	var ct time.Time
	// from times docs: ChangeTime() panics unless HasChangeTime() is true
	if ts.HasChangeTime() {
		ct = ts.ChangeTime()
	} else {
		// fall back to ModTime if ChangeTime cannot be determined
		ct = lstat.ModTime()
	}

	// returns an empty string if extension could not be determined
	// i.e. directories, filenames without extensions
	ext := filepath.Ext(path)
	newThing := &node{
		FileInfo:   lstat,
		linkState:  linkState,
		nodeName:   name,
		path:       path,
		dirCount:   -1,
		accessTime: at,
		changeTime: ct,
		ext:        ext,
		depth:      0,
	}
	return newThing, nil
}

// func insertThingsSorted(s *[]thing, t *thing) int {

//     return 0
// }

func (nav *nav) createSib(thingName string) error {

	if len(nav.menu.nodes) == 0 {
		t, err := newThing(nav.menu.path, thingName)
		if err != nil {
			return err
		}
		nav.menu.nodes = append(nav.menu.nodes, t)
		return nil
	}

	c, err := nav.currThing()
	if err != nil {
		return fmt.Errorf("opening: %s", err)
	}

	p := filepath.Dir(c.nodePath())

	t, err := newThing(p, thingName)
	if err != nil {
		return err
	}
	t.depth = c.depth

	i := nav.menu.ind
	nav.menu.nodes = append(
		nav.menu.nodes[:i],
		append(
			[]*node{t},
			nav.menu.nodes[i:]...,
		)...,
	)

	return nil
}

func (nav *nav) createChild(thingName string) error {

	if len(nav.menu.nodes) == 0 {
		return fmt.Errorf("Err: No node selected")
	}

	c, err := nav.currThing()
	if err != nil {
		return fmt.Errorf("opening: %s", err)
	}

	if c.dirPath == "" {
		dirPath := c.nodePath() + gOpts.suffix
		if err := os.Mkdir(dirPath, os.ModePerm); err != nil {
			return fmt.Errorf("Err: %s", err)
		}
		c.dirPath = dirPath

		// If node file is empty, delete it
		if lstat, err := os.Lstat(c.path); err != nil {
			return err
		} else if lstat.Size() == 0 {
			os.Remove(c.path)
			c.path = ""
		}
	}

	t, err := newThing(c.dirPath, thingName)
	if err != nil {
		return err
	}
	t.depth = c.depth + 1

	nav.expandedCache[c.dirPath] = true
	i := nav.menu.ind
	nodes := append(
		nav.menu.nodes[:i+1],
		append(
			[]*node{t},
			nav.menu.nodes[i+1:]...,
		)...,
	)

	nav.menu.nodes = nodes
	nav.down(1)

	return nil
}
